#!/bin/bash

SERVER=`hostname -I | cut -d' ' -f1` # CHANGE THIS TO MOODLE PUBIP

curl --noproxy "*" -i -X DELETE $SERVER:8083/connectors/$1 \
	        -H "Accept:application/json"

