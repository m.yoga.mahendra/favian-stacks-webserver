SERVER=`hostname -I | cut -d' ' -f1`

# rewrite datasources IP config
sed -i "s|^.*url.*$|  url: http://${SERVER}:9090|" ./grafana_datasources/prometheus_ds.yml
sed -i "s|^.*url.*$|  url: http://${SERVER}:8086|" ./grafana_datasources/influx_ds.yml

docker-compose up -d --force-recreate

